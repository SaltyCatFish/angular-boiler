(function() {
  "use strict";

  //TODO Document this
  angular
    .module('app')
    .factory('defaultService', defaultService);

  defaultService.$inject = ['$http', '$log'];

  function defaultService($http, $log) { // jshint ignore:line
    const myFunction = (value) => {
      var url = "http://www.url.com";
      $log.info("Log Message");
      return $http.get(url, {
          params: {
            value
          }
        })
        .then(response => response.data)
        .catch(error => console.error(error));
    };

    return {
      myFunction
    };
  }
})();
