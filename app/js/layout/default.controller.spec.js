// src/app/app.spec.js
// Containing describe block (or "suite"), usually named for an app feature.
// In this case the feature is the App itself.
describe('DefaultCtrl', function() {

    var vm,
        $controller,
        $scope;

    // Include Modules
    beforeEach(module('app'));
    beforeEach(inject(function(_$controller_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $controller = _$controller_('DefaultCtrl', {
            $scope: $scope
        });
    }));

    // Suite for testing an individual piece of our feature.
    describe('DefaultCtrl', function() {
        it('should be defined', function() {
            expect($controller).toBeDefined();
        });

    });
});
