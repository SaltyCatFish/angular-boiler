/*global*/

(function() {
    "use strict";
    angular
        .module('app')
        .controller('DefaultCtrl', DefaultCtrl);

    DefaultCtrl.inject = [''];

    function DefaultCtrl() {

        var vm = this;

        vm.message = "Default View";

        console.log("Hello world!");
    }
})();
